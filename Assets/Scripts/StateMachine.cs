using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tuple<T1, T2>
    {
    public T1 First {
            get; private set;
    }
    public T2 Second {
            get; private set;
    }
    internal Tuple(T1 first, T2 second)
    {
            First = first;
            Second = second;
    }
}

public class StateMachine
{
    private class State
    {
        private string name;
        private int id;
        public List<Tuple<int,Action> > prestate = new List<Tuple<int, Action> >();
        public List<Tuple<int, Action> > instate = new List<Tuple<int, Action> >();
        public List<Tuple<int, Action> > poststate = new List<Tuple<int, Action> >();

        public State(string name, int id)
        {
                this.name = name;
                this.id = id;
        }

        public State(string name, int id, int priority)
        {
                this.name = name;
                this.id = id;
        }

        private void addAction(List<Tuple<int, Action> > list, Tuple<int, Action> tuple)
        {
            int index = -1;
            for (int i=0; i<list.Count && index<0; i++)
            {
                    if (list[i].First < tuple.First)
                    {
                            index = i;
                    }
            }

            if (index < 0)
            {
                    list.Add(tuple);
            }

            else
            {
                    list.Insert(index, tuple);
            }
        }

        public void addPrestateAction(Action action)
        {
                Tuple<int, Action> tuple = new Tuple<int, Action>(50, action);

                addAction(prestate, tuple);
        }

        public void addPrestateAction(Action action, int priority)
        {
            priority = Math.Min(Math.Max(priority, 0), 100);
            Tuple<int, Action> tuple = new Tuple<int, Action>(priority, action);

            addAction(prestate, tuple);
        }

        public void addInstateAction(Action action)
        {
                Tuple<int, Action> tuple = new Tuple<int, Action>(50, action);

                addAction(instate, tuple);
        }

        public void addInstateAction(Action action, int priority)
        {
                priority = Math.Min(Math.Max(priority, 0), 100);
                Tuple<int, Action> tuple = new Tuple<int, Action>(priority, action);

                addAction(instate, tuple);
        }

        public void addPoststateAction(Action action)
        {
                Tuple<int, Action> tuple = new Tuple<int, Action>(50, action);

                addAction(poststate, tuple);
        }

        public void addPoststateAction(Action action, int priority)
        {
            priority = Math.Min(Math.Max(priority, 0), 100);
            Tuple<int, Action> tuple = new Tuple<int, Action>(priority, action);

            addAction(poststate, tuple);
    }

    public void runPrestateActions()
    {
            foreach (Tuple<int, Action> tuple in prestate)
            {
                    tuple.Second.Invoke();
            }
    }

    public void runInstateActions()
    {
            foreach (Tuple<int, Action> tuple in instate)
            {
                    tuple.Second.Invoke();
            }
    }

    public void runPoststateActions()
    {
            foreach (Tuple<int, Action> tuple in poststate)
            {
                    tuple.Second.Invoke();
            }
    }

    public string stateName()
    {
            return name;
    }

    }

    private State curState = null;
    private string name;
    private Dictionary<string, State> states = new Dictionary<string, State>();

    public StateMachine(string name)
    {
        this.name = name;
    }

    public void newState(string stateName)
    {
            states[stateName] = new State(stateName, states.Count+1);
    }

    public void addPrestateAction(string stateName, Action action)
    {
            states[stateName].addPrestateAction(action);
    }

    public void addPrestateAction(string stateName, Action action, int priority)
    {
            states[stateName].addPrestateAction(action, priority);
    }

    public void addInstateAction(string stateName, Action action)
    {
            states[stateName].addInstateAction(action);
    }

    public void addInstateAction(string stateName, Action action, int priority)
    {
            states[stateName].addInstateAction(action, priority);
    }

    public void addPoststateAction(string stateName, Action action)
    {
            states[stateName].addPoststateAction(action);
    }

    public void addPoststateAction(string stateName, Action action, int priority)
    {
            states[stateName].addPoststateAction(action, priority);
    }

    public void changeState(string stateName)
    {
            if (curState != null)
            {
                    //Debug.Log("[" + name + "] Leaving State: " + curState.stateName());
                    curState.runPoststateActions();
            }
            //Debug.Log("[" + name + "] Entering State: " + stateName);
            curState = states[stateName];
            curState.runPrestateActions();
    }

    public void update()
    {
            curState.runInstateActions();
    }

    public string state()
    {
            return curState.stateName();
    }

}
