﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class LegTest : MonoBehaviour {
private SteamVR_Controller.Device leftDevice;
private SteamVR_Controller.Device rightDevice;
public GameObject leftFoot;
public GameObject rightFoot;
public GameObject leftViveController;
public GameObject rightViveController;
private string currentLeftLegState;
private string leftTouchState;
private string currentRightLegState;
private string rightTouchState;
private string immediateTouchPadState;
// Use this for initialization
void Start () {
        //leftViveController.SetActive(false);
        //rightViveController.SetActive(false);
        leftDevice = SteamVR_Controller.Input(SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Leftmost));
        rightDevice = SteamVR_Controller.Input(SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Rightmost));
        //Set Initial State for the legs to 'legAtRest'. This means that the leg stays firmly on the current rung and is not in transtion.
        currentLeftLegState = currentRightLegState = "legAtRest";
        rightTouchState = leftTouchState = "noTouchInput";
}

// Update is called once per frame
void Update () {
        immediateTouchPadState = currentTouchPadState(leftDevice.GetAxis());
        dispatchLegStateChange("left");
        Debug.Log("Immediate = " + immediateTouchPadState + ". TouchPadState = " + leftTouchState + ". LeftLegState = " + currentLeftLegState);
}

private void dispatchLegStateChange(string leftOrRight)
{

        var newState = leftTouchState; //instantiate variable that will hold new state
        if(leftTouchState == immediateTouchPadState)
        {
                newState = leftTouchState;
        }
        else if (leftTouchState == "noTouchInput" && immediateTouchPadState == "lo")
        {
                newState = "lo";
        } else if (leftTouchState == "lo" && immediateTouchPadState == "med")
        {
                newState = "med";
        } else if (leftTouchState == "med" && immediateTouchPadState == "hi")
        {
                newState = "hi";
        } else if (leftTouchState == "hi" && immediateTouchPadState == "noTouchInput")
        {
                Debug.Log("MOVE LEG TO NEW RUNG!!!");
                newState = "noTouchInput";
        } else if(leftTouchState == "hi" && immediateTouchPadState == "med")
        {
                newState = "med";
        } else if (leftTouchState == "med" && immediateTouchPadState == "lo")
        {
                newState = "lo";
        } else
        {
                newState = "noTouchInput";
        }
        //Debug.Log("newState:" + newState);
        leftTouchState = newState;
}

private string currentTouchPadState(Vector2 xy)
{
        float yBarrier = 0.5f;
        if (xy == new Vector2(0f, 0f))
        {
                return ("noTouchInput");
        }
        else if (xy.y < -yBarrier)
        {
                return ("lo");
        }
        else if (xy.y > -yBarrier && xy.y < yBarrier)
        {
                return ("med");
        }
        else if (xy.y > yBarrier)
        {
                return ("hi");
        } else
        {
                return ("unsureOfTouchInput");
        }
}
}
