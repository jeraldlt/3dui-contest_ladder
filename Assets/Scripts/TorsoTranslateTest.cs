﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorsoTranslateTest : MonoBehaviour
{

public GameObject vive;
public GameObject leftController;
public GameObject rightController;
public GameObject leftHand;
public GameObject rightHand;
public float translationGain = 1.0f;

private SteamVR_Controller.Device leftDevice;
private SteamVR_Controller.Device rightDevice;
private Vector3 lastPosLeft;
private Vector3 lastPosRight;
private Vector3 deltaLeft;
private Vector3 deltaRight;

// Use this for initialization
void Start()
{
        leftDevice = SteamVR_Controller.Input(SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Leftmost));
        rightDevice = SteamVR_Controller.Input(SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Rightmost));

        lastPosLeft = leftController.transform.position;
        lastPosRight = rightController.transform.position;
        leftHand.transform.position = leftController.transform.position;
        rightHand.transform.position = rightController.transform.position;

}

// Update is called once per frame
void Update()
{
        // Reset
        if (leftDevice.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
        {
                leftHand.transform.position = leftController.transform.position;
                rightHand.transform.position = rightController.transform.position;
        }


        if (rightDevice.GetPress(SteamVR_Controller.ButtonMask.Grip) && rightDevice.GetPress(SteamVR_Controller.ButtonMask.Grip))
        {
                deltaLeft = leftController.transform.position - lastPosLeft;
                deltaRight = rightController.transform.position - lastPosRight;

                float deltaY = (deltaLeft.y + deltaRight.y) / -2;
                vive.transform.Translate(new Vector3(0.0f, deltaY * translationGain, 0.0f));

                if (deltaLeft.magnitude <= 0.1f)
                {
                        deltaLeft = new Vector3(deltaLeft.x, deltaLeft.y / translationGain, deltaLeft.z);
                        leftHand.transform.Translate(deltaLeft);
                }

                if (deltaRight.magnitude <= 0.1f)
                {
                        deltaRight = new Vector3(deltaRight.x, deltaRight.y / translationGain, deltaRight.z);
                        rightHand.transform.Translate(deltaRight);
                }



        }

        else
        {
                deltaLeft = leftController.transform.position - lastPosLeft;
                if (deltaLeft.magnitude <= 0.1f)
                {
                        deltaLeft = new Vector3(deltaLeft.x, deltaLeft.y * translationGain, deltaLeft.z);
                        leftHand.transform.Translate(deltaLeft);
                }

                deltaRight = rightController.transform.position - lastPosRight;
                if (deltaRight.magnitude <= 0.1f)
                {
                        deltaRight = new Vector3(deltaRight.x, deltaRight.y * translationGain, deltaRight.z);
                        rightHand.transform.Translate(deltaRight);
                }


        }




        lastPosLeft = leftController.transform.position;
        lastPosRight = rightController.transform.position;
}
}
