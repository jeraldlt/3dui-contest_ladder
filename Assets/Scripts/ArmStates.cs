﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmStates : MonoBehaviour
{
    public bool left = false;
    public float rung_distance = 0.3f;
    public GameObject controller;
    public GameObject hand;
    public GameObject handGhost;
    public GameObject colliderSmall;
    public GameObject colliderLarge;
    public List<AudioClip> releaseSounds = new List<AudioClip>();
    public List<AudioClip> graspSounds = new List<AudioClip>();


    private int cur_rung = 3;

    private GameObject colliderSmallCopy;
    private StateMachine stateMachine;
    private SteamVR_Controller.Device device;
    private AudioSource audioSource;
    private bool movingUp = false;
    private Vector3 defaultScale;
    

    private void Awake()
    {
        if (left)
        {
            stateMachine = new StateMachine("LeftArms");
        }
        else
        {
            stateMachine = new StateMachine("RightArms");
        }

        stateMachine.newState("init");
        stateMachine.newState("not_grasped");
        stateMachine.newState("grasped");

        stateMachine.addPoststateAction("init", () => this.Init_PostState());

        stateMachine.addPrestateAction("not_grasped", () => this.NotGrasped_PreState());
        stateMachine.addInstateAction("not_grasped", () => this.NotGrasped_InState());
        stateMachine.addPoststateAction("not_grasped", () => this.NotGrasped_PostState());

        stateMachine.addPrestateAction("grasped", () => this.Grasped_PreState());
        stateMachine.addInstateAction("grasped", () => this.Grasped_InState());
        stateMachine.addPoststateAction("grasped", () => this.Grasped_PostState());
    }


    void Start()
    {
        colliderSmallCopy = GameObject.Instantiate(colliderSmall);
        defaultScale = transform.localScale;
        audioSource = GetComponent<AudioSource>();
        stateMachine.changeState("init");

        moveUp();
    }


    void Update()
    {
        if (device != null && device.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
            hand.transform.position = controller.transform.position;

        stateMachine.update();
    }

    public StateMachine getStateMachine()
    {
        return stateMachine;
    }

    ////
    // init actions
    ////

    private void Init_PostState()
    {
        if (left)
            device = SteamVR_Controller.Input(SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Leftmost));
        else
            device = SteamVR_Controller.Input(SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Rightmost));
    }

    ////
    // not grasped actions
    ////

    private void NotGrasped_PreState()
    {
        Vector3 pos = colliderLarge.transform.position;
        pos.y = -1.0f;
        colliderLarge.transform.position = pos;
        transform.localScale = defaultScale;
        audioSource.PlayOneShot(RandomAudio(releaseSounds));
    }

    private void NotGrasped_InState()
    {
        if (device.GetPress(SteamVR_Controller.ButtonMask.Trigger) && colliderSmall.GetComponent<Collider>().bounds.Contains(transform.position))
        {
            movingUp = true;
            stateMachine.changeState("grasped");
        }

        else if (device.GetPress(SteamVR_Controller.ButtonMask.Trigger) && colliderSmallCopy.GetComponent<Collider>().bounds.Contains(transform.position))
        {
            movingUp = false;
            stateMachine.changeState("grasped");
        }
    }

    private void NotGrasped_PostState()
    {
        ;
    }


    ////
    // grasped actions
    ////

    private void Grasped_PreState()
    {
        transform.localScale = defaultScale * 0.7f;
        Vector3 pos = colliderLarge.transform.position;
        if (movingUp)
        {
            pos.y = (rung_distance * (cur_rung + 1));
            Vector3 posSmall = colliderSmallCopy.transform.position;
            posSmall.y = -1.0f;
            colliderSmallCopy.transform.position = posSmall;
        }
        else
        {
            pos.y = (rung_distance * (cur_rung - 1)) + (rung_distance * 0.1f);
            Vector3 posSmall = colliderSmall.transform.position;
            posSmall.y = -1.0f;
            colliderSmall.transform.position = posSmall;
        }
        colliderLarge.transform.position = pos;
        audioSource.PlayOneShot(RandomAudio(graspSounds));
    }

    private void Grasped_InState()
    {
        if (!device.GetPress(SteamVR_Controller.ButtonMask.Trigger) || !colliderLarge.GetComponent<Collider>().bounds.Contains(transform.position))
        {
            //Debug.Log("you let go!");
            device.TriggerHapticPulse(1000);
            stateMachine.changeState("not_grasped");
        }
    }

    private void Grasped_PostState()
    {
        ;
    }



    public void moveUp()
    {
        cur_rung += 1;
        Vector3 pos = colliderSmall.transform.position;
        pos.y = (rung_distance * (cur_rung + 1));
        colliderSmall.transform.position = pos;
        if (cur_rung > 4)
            pos.y = (rung_distance * (cur_rung - 1));
        else
            pos.y = -1.0f;
        colliderSmallCopy.transform.position = pos;

        pos.y = rung_distance * cur_rung;
        handGhost.transform.position = pos;
    }

    public void moveDown()
    {
        cur_rung -= 1;
        Vector3 pos = colliderSmall.transform.position;
        pos.y = (rung_distance * (cur_rung + 1));
        colliderSmall.transform.position = pos;
        pos.y = (rung_distance * (cur_rung - 1));
        colliderSmallCopy.transform.position = pos;

        pos.y = rung_distance * cur_rung;
        handGhost.transform.position = pos;
    }

    private AudioClip RandomAudio(List<AudioClip> audio)
    {

        return audio[(int)Random.Range(0.0f, (float)(audio.Count - 1))];
    }

    public void Reset(float deltaY)
    {
        transform.Translate(new Vector3(0.0f, -deltaY, 0.0f));

        cur_rung = 3;
        Vector3 pos = colliderSmall.transform.position;
        pos.y = (rung_distance * (cur_rung + 1));
        colliderSmall.transform.position = pos;
        pos.y = (rung_distance * (cur_rung - 1));
        colliderSmallCopy.transform.position = pos;

        pos = colliderLarge.transform.position;
        pos.y = -1.0f;
        colliderLarge.transform.position = pos;
        audioSource.PlayOneShot(RandomAudio(releaseSounds));

        moveUp();

        stateMachine.changeState("not_grasped");
    }

    public int GetRung()
    {
        if (stateMachine.state() == "grasped")
        {
            if (movingUp)
                return cur_rung + 1;
            else
                return cur_rung - 1;
        }

        else
            return cur_rung;
    }
}
