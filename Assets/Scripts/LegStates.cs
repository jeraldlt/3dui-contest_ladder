﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegStates : MonoBehaviour {

    public bool left = false;
    public float rung_distance = 0.3f;
    public List<AudioClip> releaseSounds = new List<AudioClip>();
    public List<AudioClip> downSounds = new List<AudioClip>();


    public int cur_rung = 0;
    private bool movingUp = false;
    private SteamVR_Controller.Device device;
    private StateMachine stateMachine;
    private AudioSource audioSource;

    private string cur_state = "";
    private float debounceTime = 0.0f;
    private float debounceTimeSuccess = 0.25f;

    void Awake()
    {
        if (left)
        {
            stateMachine = new StateMachine("LeftLegs");
        }
        else
        {
            stateMachine = new StateMachine("RightLegs");
        }

        stateMachine.newState("init");
        stateMachine.newState("idle");
        stateMachine.newState("at_rest");
        stateMachine.newState("low");
        stateMachine.newState("medium");
        stateMachine.newState("high");
        stateMachine.newState("up");
        stateMachine.newState("down");

        stateMachine.addPoststateAction("init", () => this.Init_PostState());

        stateMachine.addPrestateAction("at_rest", () => this.AtRest_PreState());
        stateMachine.addInstateAction("at_rest", () => this.AtRest_InState());
        stateMachine.addPoststateAction("at_rest", () => this.AtRest_PostState());

        stateMachine.addPrestateAction("low", () => this.Low_PreState());
        stateMachine.addInstateAction("low", () => this.Low_InState());
        stateMachine.addPoststateAction("low", () => this.Low_PostState());

        stateMachine.addPrestateAction("medium", () => this.Medium_PreState());
        stateMachine.addInstateAction("medium", () => this.Medium_InState());
        stateMachine.addPoststateAction("medium", () => this.Medium_PostState());

        stateMachine.addPrestateAction("high", () => this.High_PreState());
        stateMachine.addInstateAction("high", () => this.High_InState());
        stateMachine.addPoststateAction("high", () => this.High_PostState());

        stateMachine.addPrestateAction("up", () => this.Up_PreState());
        stateMachine.addInstateAction("up", () => this.Up_InState());
        stateMachine.addPoststateAction("up", () => this.Up_PostState());

        stateMachine.addPrestateAction("down", () => this.Down_PreState());
        stateMachine.addInstateAction("down", () => this.Down_InState());
        stateMachine.addPoststateAction("down", () => this.Down_PostState());
    }

    void Start ()
    {
        audioSource = GetComponent<AudioSource>();
        stateMachine.changeState("init");
    }

    void Update ()
    {
        stateMachine.update();
        cur_state = stateMachine.state();
    }

    public StateMachine getStateMachine()
    {
        return stateMachine;
    }

    private int getTouchpadState()
    {
        if (device == null)
        {
            return -1;
        }

        Vector2 touchpad = device.GetAxis();

        float yBarrier = 0.5f;
        if (touchpad == new Vector2(0f, 0f))
        {
            debounceTime = 0.0f;
            return -1;
        }

        else
        {
            debounceTime += Time.deltaTime;
            if (debounceTime >= debounceTimeSuccess)
            {
                if (touchpad.y < -yBarrier) // Low
                {
                    return 0;
                }
                else if (touchpad.y > -yBarrier && touchpad.y < yBarrier) // Medium
                {
                    return 1;
                }
                else if (touchpad.y > yBarrier) // High
                {
                    return 2;
                }
                else
                {
                    debounceTime = 0.0f;
                    return -1;
                }
            }

            else
            {
                return -1;
            }
        }
    }


    ////
    // init actions
    ////

    private void Init_PostState()
    {
        if (left)
            device = SteamVR_Controller.Input(SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Leftmost));
        else
            device = SteamVR_Controller.Input(SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Rightmost));
    }

    ////
    // at rest actions
    ////

    private void AtRest_PreState()
    {
        Vector3 pos = transform.position;
        pos.y = (rung_distance * cur_rung);
        transform.position = pos;
    }

    private void AtRest_InState()
    {
        if (getTouchpadState() == 0)
        {
            movingUp = true;
            stateMachine.changeState("low");
        }
        else if (getTouchpadState() == 2)
        {
            movingUp = false;
            stateMachine.changeState("high");
        }
    }

    private void AtRest_PostState()
    {
        ;
    }

    ////
    // low actions
    ////

    private void Low_PreState()
    {
        int rung = cur_rung;
        if (!movingUp)
            rung--;

        Vector3 pos = transform.position;
        pos.y = (rung_distance * rung) + (rung_distance * 0.1f);
        transform.position = pos;

        if (movingUp)
            audioSource.PlayOneShot(RandomAudio(releaseSounds));
    }

    private void Low_InState()
    {
        if (getTouchpadState() == -1)
        {
            if (!movingUp)
                stateMachine.changeState("down");
            else
                stateMachine.changeState("at_rest");
        }

        if (getTouchpadState() == 1)
        {
            stateMachine.changeState("medium");
        }
    }

    private void Low_PostState()
    {
        ;
    }

    ////
    // medium actions
    ////

    private void Medium_PreState()
    {
        int rung = cur_rung;
        if (!movingUp)
            rung--;

        Vector3 pos = transform.position;
        pos.y = (rung_distance * rung) + (rung_distance * 0.6f);
        transform.position = pos;
    }

    private void Medium_InState()
    {
        if (getTouchpadState() == -1)
        {
                stateMachine.changeState("at_rest");
        }

        if (getTouchpadState() == 0)
        {
                stateMachine.changeState("low");
        }

        if (getTouchpadState() == 2)
        {
                stateMachine.changeState("high");
        }
    }

    private void Medium_PostState()
    {
        ;
    }

    ////
    // high actions
    ////

    private void High_PreState()
    {
        int rung = cur_rung;
        if (!movingUp)
            rung--;

        Vector3 pos = transform.position;
        pos.y = (rung_distance * rung) + (rung_distance * 1.1f);
        transform.position = pos;

        if (!movingUp)
            audioSource.PlayOneShot(RandomAudio(releaseSounds));
    }

    private void High_InState()
    {
        if (getTouchpadState() == -1)
        {
            if (movingUp)
                stateMachine.changeState("up");
            else
                stateMachine.changeState("at_rest");
        }

        if (getTouchpadState() == 1)
        {
                stateMachine.changeState("medium");
        }
    }

    private void High_PostState()
    {
        ;
    }

    ////
    // up actions
    ////

    private void Up_PreState()
    {
        audioSource.PlayOneShot(RandomAudio(downSounds));
        cur_rung += 1;
        Vector3 pos = transform.position;
        pos.y = (rung_distance * cur_rung);
        transform.position = pos;
    }

    private void Up_InState()
    {
        stateMachine.changeState("at_rest");
    }

    private void Up_PostState()
    {
        ;
    }


    ////
    // down actions
    ////

    private void Down_PreState()
    {
        audioSource.PlayOneShot(RandomAudio(downSounds));
        cur_rung -= 1;
        Vector3 pos = transform.position;
        pos.y = (rung_distance * cur_rung);
        transform.position = pos;
    }

    private void Down_InState()
    {
        stateMachine.changeState("at_rest");
    }

    private void Down_PostState()
    {
        ;
    }

    private AudioClip RandomAudio(List<AudioClip> audio)
    {

        return audio[(int) Random.Range(0.0f, (float) (audio.Count-1))];
    }

    public void Reset(float deltaY)
    {
        transform.Translate(new Vector3(0.0f, -deltaY, 0.0f));

        cur_rung = 0;

        stateMachine.changeState("at_rest");
    }

    public int GetRung()
    {
        return cur_rung;
    }

    public void SetWarning(bool val)
    {
        if (val)
        {
            GetComponent<Renderer>().material.color = Color.red;
        }

        else
        {
            GetComponent<Renderer>().material.color = Color.white;
        }
    }

    public void SetSafe(bool val)
    {
        if (val)
        {
            GetComponent<Renderer>().material.color = Color.green;
        }

        else
        {
            GetComponent<Renderer>().material.color = Color.white;
        }
    }
}
