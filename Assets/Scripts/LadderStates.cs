using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LadderStates : MonoBehaviour
{

    public LegStates leftLeg;
    public LegStates rightLeg;
    public ArmStates leftArm;
    public ArmStates rightArm;
    public GameObject head;
    public Text pointsOfContact;
    public Text curState;
    public Image fadeScreen;
    public float rung_distance = 0.3f;
    public float translation_gain = 1.0f;
    public AudioClip pacman;

    private StateMachine stateMachine = new StateMachine("Ladder");
    private StateMachine leftLegStates;
    private StateMachine rightLegStates;
    private StateMachine leftArmStates;
    private StateMachine rightArmStates;
    private Vector3 lastPosLeft;
    private Vector3 lastPosRight;
    private float totalTorsoTranslation = 0.0f;
    private float fadeSpeed = 1.0f;
    private AudioSource audioSource;
    private int cur_rung = 4;

    public void Awake()
    {
        stateMachine.newState("init");
        stateMachine.newState("moving_limbs");
        stateMachine.newState("moving_torso");
        stateMachine.newState("falling");
        stateMachine.newState("falling_fade_to_black");
        stateMachine.newState("falling_fade_from_black");

        stateMachine.addInstateAction("init", () => this.Init_InState());
        stateMachine.addPoststateAction("init", () => this.Init_PostState());

        stateMachine.addPrestateAction("moving_limbs", () => this.MovingLimbs_PreState());
        stateMachine.addInstateAction("moving_limbs", () => this.MovingLimbs_InState());
        stateMachine.addPoststateAction("moving_limbs", () => this.MovingLimbs_PostState());

        stateMachine.addPrestateAction("moving_torso", () => this.MovingTorso_PreState());
        stateMachine.addInstateAction("moving_torso", () => this.MovingTorso_InState());
        stateMachine.addPoststateAction("moving_torso", () => this.MovingTorso_PostState());

        stateMachine.addPrestateAction("falling", () => this.Falling_PreState());
        stateMachine.addInstateAction("falling", () => this.Falling_InState());
        stateMachine.addPoststateAction("falling", () => this.Falling_PostState());


        stateMachine.addPrestateAction("falling_fade_to_black", () => this.Falling_FadeToBlack_PreState());
        stateMachine.addInstateAction("falling_fade_to_black", () => this.Falling_FadeToBlack_InState());
        stateMachine.addPoststateAction("falling_fade_to_black", () => this.Falling_FadeToBlack_PostState());

        stateMachine.addPrestateAction("falling_fade_from_black", () => this.Falling_FadeFromBlack_PreState());
        stateMachine.addInstateAction("falling_fade_from_black", () => this.Falling_FadeFromBlack_InState());
        stateMachine.addPoststateAction("falling_fade_from_black", () => this.Falling_FadeFromBlack_PostState());


    }

    public void Start()
    {
        audioSource = GetComponent<AudioSource>();

        leftLegStates = leftLeg.getStateMachine();
        rightLegStates = rightLeg.getStateMachine();
        leftArmStates = leftArm.getStateMachine();
        rightArmStates = rightArm.getStateMachine();

        stateMachine.changeState("init");
    }

    public void Update()
    {
        if (checkIfFalling())
            stateMachine.changeState("falling");

        curState.text = stateMachine.state();
        stateMachine.update();

        if (stateMachine.state() != "moving_torso")
        {
            Vector3 delta = leftArm.controller.transform.localPosition - lastPosLeft;
            if (delta.magnitude <= 0.1f)
            {
                delta = new Vector3(delta.x, delta.y * translation_gain, delta.z);
                leftArm.hand.transform.Translate(delta);
            }

            delta = rightArm.controller.transform.localPosition - lastPosRight;
            if (delta.magnitude <= 0.1f)
            {
                delta = new Vector3(delta.x, delta.y * translation_gain, delta.z);
                rightArm.hand.transform.Translate(delta);
            }
        }


        lastPosLeft = leftArm.controller.transform.localPosition;
        lastPosRight = rightArm.controller.transform.localPosition;
    }

    private bool checkIfFalling()
    {
        int count = 0;
        if (leftLegStates.state() == "idle" || leftLegStates.state() == "at_rest" || leftLegStates.state() == "up" || leftLegStates.state() == "down")
            count++;
        if (rightLegStates.state() == "idle" || rightLegStates.state() == "at_rest" || rightLegStates.state() == "up" || rightLegStates.state() == "down")
            count++;
        if (leftArmStates.state() == "grasped")
            count++;
        if (rightArmStates.state() == "grasped")
            count++;

        pointsOfContact.text = count + " points of contact";

        int lowest_foot = leftLeg.GetRung();
        int highest_foot = rightLeg.GetRung();
        if (lowest_foot > highest_foot)
        {
            lowest_foot = rightLeg.GetRung();
            highest_foot = leftLeg.GetRung();
        }

        int lowest_hand = leftArm.GetRung();
        int highest_hand = rightArm.GetRung();
        if (lowest_hand > highest_hand)
        {
            lowest_hand = rightArm.GetRung();
            highest_hand = leftArm.GetRung();
        }

        if (lowest_hand - highest_foot <= 2)
        {
            Debug.Log("*************Falling: hands too close to feet");
            return true;
        }

        if (highest_hand - lowest_foot >= 7)
        {
            Debug.Log("*************Falling: hands too far from feet");
            return true;
        }

        if (cur_rung == 4 || cur_rung == 14) // Either at bottom or top
        {
            leftLeg.SetSafe(true);
            rightLeg.SetSafe(true);
            return false;
        }
        else
        {
            leftLeg.SetSafe(false);
            rightLeg.SetSafe(false);
        }

        if (lowest_hand - highest_foot == 3 || lowest_hand - highest_foot == 6)
        {
            leftLeg.SetWarning(true);
            rightLeg.SetWarning(true);
        }
        else
        {
            leftLeg.SetWarning(false);
            rightLeg.SetWarning(false);
        }


        if (count < 3)
        {
            Debug.Log("*************Falling: too few points of contact");
            return true;
        }

        return false;

    }

    ////
    // Init actions
    ////

    private void Init_InState()
    {
        if (Time.realtimeSinceStartup >= 5.0f)
            stateMachine.changeState("moving_limbs");
    }

    private void Init_PostState()
    {
        lastPosLeft = leftArm.controller.transform.position;
        lastPosRight = rightArm.controller.transform.position;
        leftArm.hand.transform.position = lastPosLeft;
        rightArm.hand.transform.position = lastPosRight;

        // Change leg and arms from init
        leftLegStates.changeState("at_rest");
        rightLegStates.changeState("at_rest");
        leftArmStates.changeState("not_grasped");
        rightArmStates.changeState("not_grasped");

        //leftArm.moveUp();
        //rightArm.moveUp();
    }

    ////
    // Moving limbs actions
    ////

    private void MovingLimbs_PreState()
    {
        leftLegStates.changeState("at_rest");
        rightLegStates.changeState("at_rest");
    }

    private void MovingLimbs_InState()
    {
        float cur_hand_height = leftArm.hand.transform.position.y;
        bool correct_rung_grasped = (Mathf.Abs(cur_hand_height - ((cur_rung + 1) * rung_distance)) < 0.15 || Mathf.Abs(cur_hand_height - ((cur_rung - 1) * rung_distance)) < 0.15);
        cur_hand_height = rightArm.hand.transform.position.y;
        correct_rung_grasped = correct_rung_grasped && (Mathf.Abs(cur_hand_height - ((cur_rung + 1) * rung_distance)) < 0.15 || Mathf.Abs(cur_hand_height - ((cur_rung - 1) * rung_distance)) < 0.15);

        if (leftArmStates.state() == "grasped" && rightArmStates.state() == "grasped" && correct_rung_grasped)
        {
            stateMachine.changeState("moving_torso");
        }

        else if (Input.GetKeyDown(KeyCode.F))
        {
            stateMachine.changeState("falling");
        }
    }

    private void MovingLimbs_PostState()
    {
        ;
    }



    ////
    // Moving torso actions
    ////

    private void MovingTorso_PreState()
    {
        
        //leftLegStates.changeState("idle");
        //rightLegStates.changeState("idle");
    }

    private void MovingTorso_InState()
    {
        if (leftArmStates.state() == "grasped" && rightArmStates.state() == "grasped")
        {
            Vector3 deltaLeft = leftArm.controller.transform.localPosition - lastPosLeft;
            Vector3 deltaRight = rightArm.controller.transform.localPosition - lastPosRight;

            float deltaY = (deltaLeft.y + deltaRight.y) / 2;
            totalTorsoTranslation -= deltaY * translation_gain;
            transform.Translate(new Vector3(0.0f, deltaY * -translation_gain, 0.0f));
        }

        else
        {
            //transform.Translate(new Vector3(0.0f, -totalTorsoTranslation, 0.0f));
            stateMachine.changeState("moving_limbs");
        }

        if (totalTorsoTranslation >= rung_distance)
        {
            totalTorsoTranslation = 0.0f;
            cur_rung += 1;
            leftArm.moveUp();
            rightArm.moveUp();
            stateMachine.changeState("moving_limbs");
        }

        else if (totalTorsoTranslation <= -rung_distance)
        {
            totalTorsoTranslation = 0.0f;
            cur_rung -= 1;
            leftArm.moveDown();
            rightArm.moveDown();
            stateMachine.changeState("moving_limbs");

        }

        else if (Input.GetKeyDown(KeyCode.F))
        {
            stateMachine.changeState("falling");
        }
    }

    private void MovingTorso_PostState()
    {
        //leftLegStates.changeState("at_rest");
        //rightLegStates.changeState("at_rest");
    }




    ////
    // Falling actions
    ////

    private void Falling_PreState()
    {
        ;
    }

    private void Falling_InState()
    {
        stateMachine.changeState("falling_fade_to_black");
    }

    private void Falling_PostState()
    {
        ;
    }

    ////
    // Fading To actions
    ////
    private void Falling_FadeToBlack_PreState()
    {
        audioSource.PlayOneShot(pacman);
    }
    private void Falling_FadeToBlack_InState()
    {
        //when alpha is 1 (fully black) then transition to fading from black
        if (fadeScreen.color.a <= 0.95)
        {
            Color color = fadeScreen.color;
            color.a += fadeSpeed * Time.deltaTime;
            fadeScreen.color = color;
        }
        else
            stateMachine.changeState("falling_fade_from_black");
    }
    private void Falling_FadeToBlack_PostState()
    {
        // Call their fall method
        Vector3 pos = transform.position;
        float deltaY = pos.y;
        pos.y = 0.0f;
        transform.position = pos;
        cur_rung = 4;

        leftArm.Reset(deltaY);
        rightArm.Reset(deltaY);
        leftLeg.Reset(deltaY);
        rightLeg.Reset(deltaY);


    }

    ////
    // Fading From actions
    ////

    private void Falling_FadeFromBlack_PreState()
    {
        ;
    }
    private void Falling_FadeFromBlack_InState()
    {
        //once screen alpha is up
        if (fadeScreen.color.a >= 0.05)
        {
            Color color = fadeScreen.color;
            color.a -= fadeSpeed * Time.deltaTime;
            fadeScreen.color = color;
        }
        else
            stateMachine.changeState("moving_limbs");
    }
    private void Falling_FadeFromBlack_PostState()
    {
        ;
    }



}
